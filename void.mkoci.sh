#!/bin/sh

repoarg="-R https://alpha.de.repo.voidlinux.org/current"
pkglist="
 base-files coreutils findutils diffutils dash grep gzip
 sed mawk util-linux which tar shadow psmisc iana-etc xbps"

set -e

#
# append arguments to pkglist
#

test -z "$1" || {
	pkglist="${pkglist} $@"
}

tag="latest"
targetdir=$(mktemp -d "/tmp/umoci.XXXXXXXX")
layout="${targetdir}/void"
bundle="${targetdir}/bundle"

#
# Create a new OCI image layout and layer
#

umoci init --layout "$layout"
umoci new --image "$layout"
umoci unpack --image "$layout" "$bundle"

#
# Copy XBPS keys from host, if applicable
#

test -d /var/db/xbps/keys && {
	install -m 0755 -d "${bundle}/rootfs/var/db/xbps"
	cp -r /var/db/xbps/keys "${bundle}/rootfs/var/db/xbps/keys"
}

#
# Bootstrap the rootfs
#

case "$XBPS_ARCH" in
	*-musl)
		repoarg="${repoarg}/musl"
		pkglist="musl ${pkglist}"
		;;
esac

xbps-install -Sy ${repoarg} -r "${bundle}/rootfs" ${pkglist} >&2

xbps-reconfigure -r "${bundle}/rootfs" -vf base-files >&2
chroot "${bundle}/rootfs" env -i xbps-reconfigure -vf base-files >&2
chroot "${bundle}/rootfs" env -i xbps-reconfigure -af >&2

#
# Purge locales and documentation
#

for dir in locale doc man info groff linda; do
	rm -rf "${bundle}/rootfs/usr/share/${dir}"/*
done

#
# Purge XBPS cache files
#

rm -f "${bundle}/rootfs/var/cache/xbps"/*

#
# Repack the rootfs into a new layer and tag it
#

umoci repack --image "${layout}:${tag}" "$bundle"

printf "%s:%s\n" "${layout}" "${tag}"
