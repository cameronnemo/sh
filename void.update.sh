#!/bin/sh

# To run everyday, use this as a run script:
#
#     #!/bin/sh -e
#     exec snooze -s 1d -t /var/cache/void-update -- /path/to/this/script
#
# see man:snooze(1) for more options

set -e

msg() {
	logger -s --id=$$ -t void-update -p cron.notice "$1"
}

boot_toggle() {
	msg "Remounting /boot $1..."
	mount /boot -o remount,"$1"
}

test -z "$(command -v nm-online)" || {
	msg 'Waiting for network connection...'
	nm-online -q -x
}

msg 'Syncing repositories...'
xbps-install -S

msg 'Cleaning package cache...'
xbps-remove -yO

boot_toggle rw

msg 'Updating packages...'
xbps-install -yu

msg 'Removing orphaned packages...'
xbps-remove -yo

msg 'Purging old kernels...'
vkpurge rm all

boot_toggle ro

touch /var/cache/void-update
msg 'Update completed successfully.'
