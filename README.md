# sh

shell scripts: routine operations, useful utilities, and a flashcard game

Compiling (heh) :

    chmod +x *.sh

Installing:

    install -o root -g root -m 0755 -t /usr/local/bin *.sh

Invocation:

    apt.minimize.sh
    cgfs.user.sh	USER
    debian.minimize.sh
    flashcards.sh	FILE
    mkswap.sh		[SIZE]
    void.mkoci.sh	[PACKAGE...]
    void.update.sh	[TIMEFILE]
    xdg.rundir.sh	USER
    xhost.root.sh	[off]

## apt.minimize.sh

Decrease the footprint of an apt/dpkg installation

Removes apt/dpkg related files that can be redownloaded and are thus extraneous.

Used in debian family containers to cut down on disk space usage.

Must be re-run after any `apt update` or similar procedure.

## debian.minimize.sh

removes documentation and locale files on a debian system

optionally excludes the locale specified in `/etc/default/locale` or
`/etc/locale.gen` (with the `$LANG` variable)

## flashcards.sh

Parses a plaintext file (two+ columns, word on column one, definition on rest
of columns). You say whether you knew the word (after or before seeing the
definition) by typing in "y" and pressing enter.

Words you did not know (type in anything or nothing, then press enter) will be
added to the list for the next round. At the end, it tells you how many rounds
it took to get through the entire list.

## mkswap.sh

Creates a swapfile at `/swapfile`. The size can be supplied in KB by command
line argument. Otherwise, it is chosen from the lesser of 2 GB or the size
of ram on the host.

Appends a relevant line to the fstab.

Take caution if your root partition is small.

## void.mkoci.sh

Creates an OCI image, with optional additional packages to be installed into the
image. You must run it inside of a user namespace, using e.g. `lxc-usernsexec`.

    lxc-usernsexec -- void-mkoci bash runit-void socklog python3

Prints the image location to stdout. You can then copy it to a container repo
like DockerHub using a tool such as skopeo:

    skopeo copy oci:IMAGE docker://USER/REPO

## void.update.sh

Updates a running, network connected Void Linux installation, removes obsolete
packages, purges the cache, and removes old and unused kernels.

The script uses a timefile to record time of last run (default:
`/var/cache/void-update`). The timefile can be used with `snooze` and a service
manager like `runit` to enable the script as a periodic task (similar to cron).

## xdg.rundir.sh

Creates an `$XDG_RUNTIME_DIR` (tmpfs) for the user specified by the mandatory
argument, `USER`, with the pattern `/run/user/$UID/`.

Probably inferior to [pam\_rundir](https://jjacky.com/pam_rundir/).

## cgfs.user.sh

Creates a cgroup hierarchy for the user specified in the freezer and memory
controllers. The user can then move any of their processes into the newly
created hierarchy and launch unpriveleged LXC containers.

Probably inferior to
[pam\_cgfs](https://github.com/lxc/lxc/blob/master/src/lxc/pam/pam_cgfs.c).

## xhost.root.sh

This will allow root applications to run on XWayland (e.g. gparted). It sets
access control lists to permit local root users. Invoking the program with the
`off` argument will undo the ACL change.
