#!/bin/sh

set -eu

user="$1"

for cgrp in freezer memory; do
	dir="/sys/fs/cgroup/${cgrp}/${user}"
	mkdir -p "${dir}"
	chown -R "${user}:${user}" "${dir}"
done
