#!/bin/sh

set -e

usage() {
	echo "Usage: mkswap.sh [-h | --help] [swap-size]"
	echo ""
	echo "	swap-size: explicitly set size of swap in kB"
	echo "	           (defaults to lesser of size of ram or 2 GB)"
}

case $1 in
	-h|--help) { usage; exit 0; };;
	*) swap_size="$1" ;;
esac

get_ram_size() {
	test -r /proc/meminfo || { echo "ERROR: /proc/meminfo not readable, could not determine RAM size"; exit 1; }
	ram=$(awk '{ if ($1 == "MemTotal:") print $2 };' /proc/meminfo | head -n1)
	test -n "$ram" || { echo "ERROR: could not determine RAM size"; exit 1; }
	test "$ram" -le "2000000" || ram="2000000"
	echo "$ram"
	return
}

test -n "$swap_size" || swap_size="$(get_ram_size)"

dd if=/dev/zero of=/swapfile bs=1024 count="$swap_size"
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile

echo "/swapfile	swap		swap	defaults	0	0" >> /etc/fstab
